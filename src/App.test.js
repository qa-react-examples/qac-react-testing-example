import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

describe('App', () => {
  let component;
  let instance;

  beforeEach(() => {
    component = renderer.create(
      <App />
    );
    instance = component.getInstance();
  });

  it('renders without crashing', () => {
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should increment the counter when incrementCounter is called', () => {
    component.root.findByType('button').props.onClick();
    expect(instance.state.counter).toEqual(1); // testing the instance directly
    expect(component.toJSON()).toMatchSnapshot(); // testing against a jest snapshot
  });

});


