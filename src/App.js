import React from 'react';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0
    };

    this.incrementCounter = this.incrementCounter.bind(this);
  }

  incrementCounter() {
    this.setState({counter: this.state.counter + 1});
  }
  
  exampleFunctionThatReturnsSomething(data) {
    // This function just returns what it receives
    return data;
  }

  render() {
    return (
      <div>
        <p id="count">
        {this.state.counter}
        </p>
        <button onClick={this.incrementCounter}>Increment</button>
      </div>
    )
  }
}
